<?php

return [
	'loginform' => [
		'placeholder_email' => 'username',
		'placeholder_password' => 'password',
		'remember_me' => 'remember',
	],
];
