<?php

return [
	'loginform' => [
		'placeholder_email' => 'gebruikersnaam',
		'placeholder_password' => 'wachtwoord',
		'remember_me' => 'onthouden',
	],
];
